﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct NeuronLayer
{
    public int numNeurons;
    public Neuron[] neurons;

    public void CreateNeuronLayer(int _numNeurons, int numInputsPerNeuron)
    {
        numNeurons = _numNeurons;
        neurons = new Neuron[numNeurons];
        for (int i = 0; i < numNeurons; i++)
        {
            Neuron n = new Neuron();
            n.CreateNeuron(numInputsPerNeuron);
            neurons[i] = n;
        }
    }
}
