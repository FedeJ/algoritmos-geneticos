﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Neuron
{
    public int numInputs;
    public float[] inputWeights;

    public void CreateNeuron(int _numInputs)
    {
        //The +1 is for the bias
        numInputs = _numInputs;
        inputWeights = new float[numInputs + 1];
        for (int i = 0; i < numInputs + 1; i++)
        {
            inputWeights[i] = Random.Range(-1.0f, 1.0f);
        }
    }
}
