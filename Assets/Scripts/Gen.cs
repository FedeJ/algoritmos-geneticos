﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Gen
{
    public enum Action { Adelante, Atras, Izquierda, Derecha, Arriba }
    public Action accion;
    public float tiempo;

    public void CreateSimpleGen()
    {
        accion = (Action)Random.Range(0, System.Enum.GetValues(typeof(Action)).Length);
        tiempo = Random.Range(0.2f, 1.0f);
    }
}
