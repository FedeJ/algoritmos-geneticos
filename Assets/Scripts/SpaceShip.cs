﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShip : MonoBehaviour
{
    public float force = 2;

    public Transform closestObjective;

    public float lastPoint;
    public float lastDistance;

    public ParticleSystem psAdelante;
    public ParticleSystem psAtras;
    public ParticleSystem psDerecha;
    public ParticleSystem psIzquierda;
    public ParticleSystem psArriba;

    [HideInInspector]
    public Agent agente; //Tiene Referencia

    [SerializeField]
    private int genIndex = 0;
    private Rigidbody rb;

    private float t1;

    //private float distanceStart;
    private float lifeTime;
    public bool wasCloseToObjetive;
    public float puntaje;


    private void Awake()
    {

        //distanceStart = Vector3.Distance(transform.position, posObjetivo);
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        if (agente == null)
            return;

        lastPoint = agente.cromosoma.puntaje;
        agente.cromosoma.puntaje = 0;

        if (closestObjective == null)
        {
            closestObjective = Manager.instancia.objetivos[0].transform;
        }
    }

    private void FixedUpdate()
    {
        if (agente == null)
            return;

        t1 += Time.fixedDeltaTime;
        switch (Manager.instancia.currentIaType)
        {
            case Manager.IaType.GeneticAlgorithm:
                PropulsionSimple();
                break;
            case Manager.IaType.NeuralNetwork:
                PropulsionNeuronal();
                break;
            default:
                break;
        }

        closestObjective = ObjectivoMasCercano();
        CalcularPuntaje();
        lifeTime += Time.fixedDeltaTime;
        if (lifeTime > 15)
        {
            Manager.instancia.spaceShipsList.Remove(this);
            Destroy(gameObject);
        }
    }

    public Transform ObjectivoMasCercano()
    {
        float masCercano = Vector3.Distance(transform.position, closestObjective.position);
        Transform closest = closestObjective;
        foreach (var item in Manager.instancia.objetivos)
        {
            if (Vector3.Distance(transform.position, item.transform.position) < masCercano)
            {
                masCercano = Vector3.Distance(transform.position, item.transform.position);
                closest = item.transform;
            }
        }
        return closest;
    }

    void CalcularPuntaje()
    {
        var distance = Vector3.Distance(transform.position, closestObjective.position);
        /*
        var color = Color.Lerp(Color.green, Color.red, distance / distanceStart);
        Debug.DrawLine(transform.position, posObjetivo, color);
        */

        switch (Manager.instancia.currentIaType)
        {
            case Manager.IaType.GeneticAlgorithm:
                agente.cromosoma.puntaje = (1000 / (distance + 1) / rb.velocity.magnitude);
                puntaje = agente.cromosoma.puntaje;
                break;
            case Manager.IaType.NeuralNetwork:
                if (transform.position.y > closestObjective.position.y)
                {
                    agente.cromosoma.puntaje += (1000 / (distance + 1));
                    puntaje = agente.cromosoma.puntaje;
                }
                break;
        }

    }

    void PropulsionNeuronal()
    {

        var vectorMov = Vector3.Normalize(rb.velocity);
        var vectorToLook = Vector3.Normalize(closestObjective.position - transform.position);

        float[] inputs = new float[6]
        {
            vectorMov.x,
            vectorMov.y,
            vectorMov.z,
            vectorToLook.x,
            vectorToLook.y,
            vectorToLook.z
        };

        var outputs = agente.cromosoma.neuralNet.Update(inputs);

        var horizontal = outputs[0] - outputs[1];
        var vertical = outputs[2] - outputs[3];
        var height = outputs[4];

        rb.AddForce((Vector3.forward * force * horizontal) + (Vector3.right * force * vertical) + (Vector3.up * force * height), ForceMode.Force);

        Debug.DrawRay(transform.position, vectorMov * 3, Color.magenta);
        Debug.DrawRay(transform.position, vectorToLook * 3, Color.green);

        if (rb.velocity.magnitude > 8)
        {
            rb.velocity = rb.velocity.normalized * 8;
        }
    }

    void PropulsionSimple()
    {
        if (t1 >= agente.cromosoma.genes[genIndex].tiempo && genIndex + 1 < agente.cromosoma.genes.Count)
        {
            t1 = 0;

            psAdelante.Stop();
            psAtras.Stop();
            psDerecha.Stop();
            psIzquierda.Stop();
            psArriba.Stop();

            switch (agente.cromosoma.genes[genIndex].accion)
            {
                case Gen.Action.Adelante:
                    psAdelante.Play();
                    break;
                case Gen.Action.Atras:
                    psAtras.Play();
                    break;
                case Gen.Action.Derecha:
                    psDerecha.Play();
                    break;
                case Gen.Action.Izquierda:
                    psIzquierda.Play();
                    break;
                case Gen.Action.Arriba:
                    psArriba.Play();
                    break;
            }

            genIndex++;
        }
        else if (genIndex + 1 >= agente.cromosoma.genes.Count)
        {
            genIndex = 0;
        }

        switch (agente.cromosoma.genes[genIndex].accion)
        {
            case Gen.Action.Adelante:
                //transform.Translate(Vector3.forward * 10 * Time.deltaTime);
                rb.AddForce(Vector3.forward * force, ForceMode.Force);
                break;
            case Gen.Action.Atras:
                //transform.Translate(-Vector3.forward * 10 * Time.deltaTime);
                rb.AddForce(-Vector3.forward * force, ForceMode.Force);
                break;
            case Gen.Action.Derecha:
                //transform.Translate(Vector3.right * 10 * Time.deltaTime);
                rb.AddForce(Vector3.right * force, ForceMode.Force);
                break;
            case Gen.Action.Izquierda:
                //transform.Translate(-Vector3.right * 10 * Time.deltaTime);
                rb.AddForce(-Vector3.right * force, ForceMode.Force);
                break;
            case Gen.Action.Arriba:
                //transform.Translate(-Vector3.up * 10 * Time.deltaTime);
                rb.AddForce(Vector3.up * 3 * force, ForceMode.Force);
                break;
        }
    }
}
