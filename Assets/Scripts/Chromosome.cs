﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chromosome
{

    public float puntaje; // Lo calcula el agente y y lo guarda el cromosoma
    public List<Gen> genes; // Cromosoma los Crea
    public NeuralNet neuralNet;
    public bool elite = false;

    public Chromosome()
    {
        CrearGenes();
    }

    void CrearGenes()
    {
        switch (Manager.instancia.currentIaType)
        {
            case Manager.IaType.GeneticAlgorithm:
                genes = new List<Gen>();
                for (int i = 0; i < Manager.instancia.chromosomeSize; i++)
                {
                    Gen gen = new Gen();
                    gen.CreateSimpleGen();
                    genes.Add(gen);
                }
                break;
            case Manager.IaType.NeuralNetwork:
                neuralNet = new NeuralNet();
                neuralNet.CreateNet();
                break;
        }
    }
}
