﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct NeuralNet
{
    public float bias;
    public float pendiente;
    public int numInputs;
    public int numOutputs;
    public int numHiddenLayers;
    public int neuronsPerHiddenLayer;

    public List<NeuronLayer> neuronLayers;

    public void CreateNet()
    {
        bias = Manager.instancia.bias;
        pendiente = Manager.instancia.pendiente;
        neuronsPerHiddenLayer = Manager.instancia.neuronsPerHiddenLayer;
        numHiddenLayers = Manager.instancia.numHiddenLayers;
        numInputs = Manager.instancia.numInputs;
        numOutputs = Manager.instancia.numOutputs;

        neuronLayers = new List<NeuronLayer>();
        NeuronLayer nL; //it's a struct

        if (numHiddenLayers > 0) //In case there is hidden layer
        {
            //Add Inputs Layer
            nL = new NeuronLayer();
            nL.CreateNeuronLayer(neuronsPerHiddenLayer, numInputs); //first hidden layer connects with inputs
            neuronLayers.Add(nL);

            for (int i = 1; i < numHiddenLayers; i++)
            {
                //Add Hidden Layers
                nL = new NeuronLayer();
                nL.CreateNeuronLayer(neuronsPerHiddenLayer, neuronsPerHiddenLayer);
                neuronLayers.Add(nL);
            }
            //Add OutputLayer
            nL = new NeuronLayer();
            nL.CreateNeuronLayer(numOutputs, neuronsPerHiddenLayer);
            neuronLayers.Add(nL);
        }
        else //If there is not hidden layer
        {
            //Add Inputs Layer
            nL = new NeuronLayer();
            nL.CreateNeuronLayer(numOutputs, numInputs);
            neuronLayers.Add(nL);
        }
    }

    public float Sigmoid(float activation)
    {
        return (1.0f / (1.0f + Mathf.Pow(Mathf.Epsilon, -activation / pendiente)));
    }

    public float[] Update(float[] inputs)
    {
        //stores the resultant outputs from each layer
        List<float> outputs = new List<float>();

        if (inputs.Length != Manager.instancia.numInputs)
        {
            Debug.LogError("Cantidad de inputs incorrecta");
            return new float[0];
        }

        //For each layer...
        for (int i = 0; i < numHiddenLayers + 1 /* + outputLayer  */; i++)
        {
            if (i > 0)
            {
                inputs = outputs.ToArray();
            }
            outputs.Clear();
            //for each neuron sum the inputs * corresponding weights. Throw
            //the total at the sigmoid function to get the output
            for (int j = 0; j < neuronLayers[i].numNeurons; j++)
            {
                
                float activation = 0;
                var neuron = neuronLayers[i].neurons[j];

                for (int k = 0; k < neuron.numInputs; k++)
                {
                    activation += neuronLayers[i].neurons[j].inputWeights[k] * inputs[k];
                }
                activation += neuronLayers[i].neurons[j].inputWeights[neuron.numInputs] * bias;
                outputs.Add(Sigmoid(activation));
            }
        }
        return outputs.ToArray();
    }
}