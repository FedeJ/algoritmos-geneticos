﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Manager))]
public class ManagerEditor : Editor
{
    public SerializedProperty
    currentIaType_Prop,
    cantAgentes_Prop,
    cantElites_Prop,
    chromosomeSize_Prop,
    forceOfShip_Prop,
    speedOfSimulation_Prop,
    bias_Prop,
    pendiente_Prop,
    numInputs_Prop,
    numOutputs_Prop,
    numHiddenLayers_Prop,
    neuronsPerHiddenLayer_Prop;

    void OnEnable()
    {
        currentIaType_Prop = serializedObject.FindProperty("currentIaType");
        cantAgentes_Prop = serializedObject.FindProperty("cantAgentes");
        cantElites_Prop = serializedObject.FindProperty("cantElites");
        chromosomeSize_Prop = serializedObject.FindProperty("chromosomeSize");
        forceOfShip_Prop = serializedObject.FindProperty("forceOfShip");
        speedOfSimulation_Prop = serializedObject.FindProperty("speedOfSimulation");


        bias_Prop = serializedObject.FindProperty("bias");
        pendiente_Prop = serializedObject.FindProperty("pendiente");
        numInputs_Prop = serializedObject.FindProperty("numInputs");
        numOutputs_Prop = serializedObject.FindProperty("numOutputs");
        numHiddenLayers_Prop = serializedObject.FindProperty("numHiddenLayers");
        neuronsPerHiddenLayer_Prop = serializedObject.FindProperty("neuronsPerHiddenLayer");
    }
    public override void OnInspectorGUI()
    {
        Manager manager = (Manager)target;
        serializedObject.Update();

        EditorGUILayout.PropertyField(forceOfShip_Prop);
        EditorGUILayout.PropertyField(speedOfSimulation_Prop);
        EditorGUILayout.PropertyField(cantAgentes_Prop);
        EditorGUILayout.PropertyField(cantElites_Prop);
        EditorGUILayout.PropertyField(currentIaType_Prop);


        if (manager.currentIaType == Manager.IaType.NeuralNetwork)
        {
            EditorGUILayout.PropertyField(bias_Prop);
            EditorGUILayout.PropertyField(pendiente_Prop);
            EditorGUILayout.PropertyField(numInputs_Prop);
            EditorGUILayout.PropertyField(numOutputs_Prop);
            EditorGUILayout.PropertyField(numHiddenLayers_Prop);
            EditorGUILayout.PropertyField(neuronsPerHiddenLayer_Prop);
        }
        else
        {
            EditorGUILayout.PropertyField(chromosomeSize_Prop);
        }

        serializedObject.ApplyModifiedProperties();
    }
}

