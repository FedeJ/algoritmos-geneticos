﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GeneticAlgorithm
{

    public List<Chromosome> chromosomes;
    public List<Chromosome> newChromosomes;

    public Manager manager;

    private float totalScore = 0;
    private int splitPoint;

    public void StartNextGeneration()
    {
        totalScore = 0;
        float bestScore = 0;
        foreach (var chromosome in chromosomes)
        {
            totalScore += chromosome.puntaje;
            if (chromosome.puntaje > bestScore)
            {
                bestScore = chromosome.puntaje;
            }
        }

        Debug.Log("Total Score: " + totalScore + "  //  Avg Score: " + totalScore / chromosomes.Count + "  //  Best Score: " + bestScore);

        newChromosomes = new List<Chromosome>();

        Elitism();

        for (int i = manager.cantElites / 2; i < chromosomes.Count / 2; i++)
        {
            //splitPoint = (int)Random.Range(manager.chromosomeSize / 3, manager.chromosomeSize - manager.chromosomeSize / 3);
            if (manager.numHiddenLayers > 0)
            {
                splitPoint = Random.Range(0, manager.neuronsPerHiddenLayer * manager.numHiddenLayers + manager.numHiddenLayers * manager.numOutputs);
            }
            else
            {
                splitPoint = Random.Range(0, manager.numOutputs);
            }
            Chromosome dad = Roulette();
            Chromosome mom = Roulette();
            newChromosomes.Add(CrossOver(dad, mom));
            newChromosomes.Add(CrossOver(mom, dad));
        }

        chromosomes = newChromosomes;

        manager.CreatePopulation();
    }

    //Funcion CrossOver
    //Agarramos la mitad del padre y madre y sacamos 2 hojos de hechos [P/M] y [M/P]
    Chromosome CrossOver(Chromosome c1, Chromosome c2)
    {
        //Create a child
        Chromosome c = new Chromosome();
        bool turn = false;

        //Blender Time
        switch (manager.currentIaType)
        {
            case Manager.IaType.GeneticAlgorithm:
                #region metodo1
                /*
                for (int i = 0; i < c.genes.Count; i++)
                {
                    if (turn)
                        c.genes[i] = c1.genes[i];
                    else
                        c.genes[i] = c2.genes[i];
                    turn = !turn;
                }
                */
                #endregion
                #region metodo2

                int value = 0;
                for (int i = 0; i < c.genes.Count; i++)
                {
                    if (turn)
                        c.genes[i] = c1.genes[i];
                    else
                        c.genes[i] = c2.genes[i];

                    //Cada tres cambia
                    value++;
                    if (value >= 3)
                    {
                        turn = !turn;
                        value = 0;
                    }
                }
                #endregion
                #region metodo3
                /*
                for (int i = 0; i < c.genes.Count / 2; i++)
                {
                    c.genes[i] = c1.genes[i];
                }

                for (int i = c.genes.Count / 2; i < c.genes.Count; i++)
                {
                    c.genes[i] = c2.genes[i];
                }
                */
                #endregion
                break;
            case Manager.IaType.NeuralNetwork:
                int weightsAssigned = 0;
                int neuronsCounted = 0;
                for (int i = 0; i < c.neuralNet.neuronLayers.Count; i++)
                {
                    for (int j = 0; j < c.neuralNet.neuronLayers[i].neurons.Length; j++)
                    {
                        neuronsCounted++;
                        if (splitPoint < neuronsCounted)
                        {
                            turn = true;
                        }
                        for (int k = 0; k < c.neuralNet.neuronLayers[i].neurons[j].inputWeights.Length; k++)
                        {
                            weightsAssigned++;
                            if (turn)
                            {
                                c.neuralNet.neuronLayers[i].neurons[j].inputWeights[k] = c1.neuralNet.neuronLayers[i].neurons[j].inputWeights[k];
                                //Debug.Log("C1 " + c1.neuralNet.neuronLayers[i].neurons[j].inputWeights[k]);
                            }
                            else
                            {
                                c.neuralNet.neuronLayers[i].neurons[j].inputWeights[k] = c2.neuralNet.neuronLayers[i].neurons[j].inputWeights[k];
                                //Debug.Log("C2 " + c2.neuralNet.neuronLayers[i].neurons[j].inputWeights[k]);
                            }
                            //Debug.Log("C " + c.neuralNet.neuronLayers[i].neurons[j].inputWeights[k]);
                        }
                    }
                }
                break;
        }

        c = Mutation(c);
        return c;
    }

    //Funcion Roulette 
    //Seleccion de padres por ruletas, cuantos mas puntaje mas grande su porcion en la ruleta
    Chromosome Roulette()
    {
        var roulettePos = Random.Range(0, totalScore);

        float scoreReached = 0;
        foreach (var chromosome in chromosomes)
        {
            scoreReached += chromosome.puntaje;
            if (roulettePos <= scoreReached)
            {
                return chromosome;
            }
        }
        Debug.LogError("My Log: Error en ruleta");
        return new Chromosome();
    }

    //Funcion Mutation
    //Modificar un poco por chance de mutacion a cada gen
    Chromosome Mutation(Chromosome c)
    {
        switch (manager.currentIaType)
        {
            case Manager.IaType.GeneticAlgorithm:
                for (int i = 0; i < c.genes.Count; i++)
                {
                    Gen gen = new Gen();
                    gen = c.genes[i];

                    if (Random.Range(0, 100) < 1)
                    {
                        //Debug.Log("Muted Time");
                        gen.tiempo += Random.Range(-0.5f, 0.5f);
                    }
                    if (Random.Range(0, 100) > 98)
                    {
                        //Debug.Log("Muted Action");
                        gen.accion = (Gen.Action)Random.Range(0, System.Enum.GetValues(typeof(Gen.Action)).Length);
                    }
                    c.genes[i] = gen;
                }
                break;
            case Manager.IaType.NeuralNetwork:
                for (int i = 0; i < c.neuralNet.neuronLayers.Count; i++)
                {
                    for (int j = 0; j < c.neuralNet.neuronLayers[i].neurons.Length; j++)
                    {
                        for (int k = 0; k < c.neuralNet.neuronLayers[i].neurons[j].inputWeights.Length; k++)
                        {
                            if (Random.Range(0, 100) < 5)
                            {
                                c.neuralNet.neuronLayers[i].neurons[j].inputWeights[k] += Random.Range(-5.0f, 0.5f);
                                c.neuralNet.neuronLayers[i].neurons[j].inputWeights[k] = Mathf.Clamp(c.neuralNet.neuronLayers[i].neurons[j].inputWeights[k], 0.0f, 1.0f);
                            }
                        }

                    }
                }
                break;
        }
        return c;
    }

    //Funcion Elitism
    //Se elije a los mejores resultados
    private void Elitism()
    {
        chromosomes = chromosomes.OrderByDescending(o => o.puntaje).ToList();
        for (int i = 0; i < manager.cantElites; i++)
        {
            chromosomes[i].elite = true;
            newChromosomes.Add(chromosomes[i]);
        }
    }
}
