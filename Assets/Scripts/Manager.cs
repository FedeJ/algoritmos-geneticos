﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class Manager : MonoBehaviour
{

    public static Manager instancia;

    private GameObject spaceShipPrefab;
    private Material eliteMat;

    private Text currentGenerationText;
    private int currentGeneration = 0;

    public enum IaType { GeneticAlgorithm, NeuralNetwork }
    public IaType currentIaType;

    public int cantAgentes;
    public int cantElites;

    public float chromosomeSize;

    [Range(1, 10)]
    public float forceOfShip;

    [Range(1, 10)]
    public float speedOfSimulation;

    public float bias = 1;
    public float pendiente = 1;
    public int numInputs;
    public int numOutputs;
    public int numHiddenLayers;
    public int neuronsPerHiddenLayer;

    [HideInInspector]
    public List<SpaceShip> spaceShipsList;
    private Transform shipSpawn;
    private float fixedDelta;
    private GeneticAlgorithm genAlg;
    private List<Agent> agentes;
    private bool restarting;
    private int posIndex;

    public GameObject[] objetivos;

    private void Start()
    {
        objetivos = GameObject.FindGameObjectsWithTag("Objetive");
        fixedDelta = Time.fixedDeltaTime;
    }

    private void FixedUpdate()
    {
        Time.timeScale = speedOfSimulation;
        Time.fixedDeltaTime = fixedDelta / Time.timeScale;

        if (spaceShipsList.Count <= 0 && !restarting)
        {
            restarting = true;
            Invoke("StartNextGeneration", 0.5f);
        }
    }

    public void CreatePopulation()
    {
        //mezcla las posiciones de los cromosomas en la lista de cromosomas
        for (int i = 0; i < genAlg.chromosomes.Count; i++)
        {
            var temp = genAlg.chromosomes[i];
            int randomIndex = Random.Range(i, genAlg.chromosomes.Count);
            genAlg.chromosomes[i] = genAlg.chromosomes[randomIndex];
            genAlg.chromosomes[randomIndex] = temp;
        }

        spaceShipsList.Clear();
        int elitesCoulored = 0;
        currentGeneration++;
        currentGenerationText.text = currentGeneration.ToString();

        for (int i = 0; i < cantAgentes; i++)
        {
            GameObject go = null;
            switch (currentIaType)
            {
                case IaType.GeneticAlgorithm:
                    go = Instantiate(spaceShipPrefab, shipSpawn.position, Quaternion.identity);
                    break;
                case IaType.NeuralNetwork:
                    //go = Instantiate(spaceShipPrefab, /*Random.insideUnitSphere* 40 +*/ shipSpawn.position, Quaternion.identity);
                    if (posIndex >= 4)
                        posIndex = 0;
                    int a = i * 360 /cantAgentes;
                    Vector3 pos = RandomCircle(shipSpawn.position, 50, a);
                    go = Instantiate(spaceShipPrefab, pos, Quaternion.identity);
                    posIndex++;  
                    break;
                default:
                    go = Instantiate(spaceShipPrefab, shipSpawn.position, Quaternion.identity);
                    break;
            }
            if (go == null)
            {
                return;
            }
            var spaceShip = go.GetComponent<SpaceShip>();

            spaceShip.agente = agentes[i];
            spaceShip.agente.cromosoma = genAlg.chromosomes[i];

            //Aplico color de elite
            if (spaceShip.agente.cromosoma.elite)
            {
                spaceShip.transform.Find("Capsula").GetComponent<MeshRenderer>().material = eliteMat;
                spaceShip.gameObject.name = "EliteSpaceShip";
                elitesCoulored++;
            }

            spaceShip.force = forceOfShip;

            //Debug.Log(spaceShip.agente.cromosoma.puntaje + " " + spaceShip.agente.cromosoma.ID + " " + spaceShip.agente.cromosoma.genes[0].accion, spaceShip.gameObject);

            spaceShipsList.Add(spaceShip);
        }
    }

    private void Awake()
    {
        if (instancia)
        {
            Destroy(gameObject);
            return;
        }
        instancia = this;

        Application.runInBackground = true;

        spaceShipPrefab = (GameObject)Resources.Load("Nave");
        eliteMat = (Material)Resources.Load("Elite");
        currentGenerationText = GameObject.Find("NumGenerationText").GetComponent<Text>();

        //de -9.81 a gravedad deseada
        Physics.gravity = new Vector3(0, -9.0f, 0);

        genAlg = new GeneticAlgorithm();
        spaceShipsList = new List<SpaceShip>();

        genAlg.chromosomes = new List<Chromosome>();
        agentes = new List<Agent>();

        if (currentIaType == IaType.NeuralNetwork)
        {
            if (numHiddenLayers > 0)
                //(L-1)(N^2+N) + I.N + N + N.O + O
                chromosomeSize = (numHiddenLayers - 1) * (Mathf.Pow(neuronsPerHiddenLayer, 2) + 2) + numInputs * neuronsPerHiddenLayer + neuronsPerHiddenLayer + neuronsPerHiddenLayer * numOutputs + numOutputs;
            else
                chromosomeSize = numOutputs * numInputs + numOutputs;
            print("Quantity of Weights: " + chromosomeSize);
        }

        genAlg.manager = this;
        for (int i = 0; i < cantAgentes; i++)
        {
            Agent agente = new Agent();
            Chromosome chromosome = new Chromosome();
            agente.cromosoma = chromosome;
            agentes.Add(agente);
            genAlg.chromosomes.Add(chromosome);
        }
        shipSpawn = GameObject.FindGameObjectWithTag("Base").transform;
        CreatePopulation();
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            spaceShipsList.Remove(other.GetComponent<SpaceShip>());
            Destroy(other.gameObject);
        }
    }

    public void StartNextGeneration()
    {
        restarting = false;
        genAlg.StartNextGeneration();
    }

    Vector3 RandomCircle(Vector3 center, float radius, int a)
    {
        float ang = a;
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.z = center.z + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.y = center.y;
        return pos;
    }
}
